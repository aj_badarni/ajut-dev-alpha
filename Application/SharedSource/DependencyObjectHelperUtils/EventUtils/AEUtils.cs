﻿namespace AJut.Application
{
    using System;
    using System.Diagnostics;
    using System.Windows;

    public class AEUtilsRegistrationHelper
    {
        private readonly Type m_ownerType;
        public AEUtilsRegistrationHelper (Type ownerType)
        {
            m_ownerType = ownerType;
        }

        public RoutedEvent RegisterRoutedEvent<TRoutedEventHandler> (StaticAEUtils.AttachedEventHandler<TRoutedEventHandler> addFunc, StaticAEUtils.AttachedEventHandler<TRoutedEventHandler> removeFunc, RoutingStrategy routingStrategy = RoutingStrategy.Bubble)
        {
            return StaticAEUtils.RegisterRoutedEvent(m_ownerType, addFunc, removeFunc, routingStrategy);
        }

        public RoutedEvent RegisterRoutedEvent (StaticAEUtils.AttachedEventHandler<RoutedEventHandler> addFunc, StaticAEUtils.AttachedEventHandler<RoutedEventHandler> removeFunc, RoutingStrategy routingStrategy = RoutingStrategy.Bubble)
        {
            return StaticAEUtils.RegisterRoutedEvent(m_ownerType, addFunc, removeFunc, routingStrategy);
        }
    }

    public static class StaticAEUtils
    {
        public delegate void AttachedEventHandler<TRoutedEventHandler> (DependencyObject obj, TRoutedEventHandler handler);
        public static RoutedEvent RegisterRoutedEvent<TRoutedEventHandler> (Type ownerType, AttachedEventHandler<TRoutedEventHandler> addFunc, AttachedEventHandler<TRoutedEventHandler> removeFunc, RoutingStrategy routingStrategy = RoutingStrategy.Bubble)
        {
            DebugValidateHandlers(addFunc, removeFunc);

            // Add{Name}Handler ---- Magic Numbers: Add = 3 letters, Add+Handler = 10 Characters
            string name = addFunc.Method.Name.Substring(3, addFunc.Method.Name.Length - 10);
            return EventManager.RegisterRoutedEvent(name, routingStrategy, typeof(TRoutedEventHandler), ownerType);
        }

        [Conditional("DEBUG")]
        public static void DebugValidateHandlers<TRoutedEventHandler> (AttachedEventHandler<TRoutedEventHandler> addFunc, AttachedEventHandler<TRoutedEventHandler> removeFunc)
        {
            if (addFunc == null)
            {
                throw new Exception("AEUtils - Error With Adder: Attached Events need add functions");
            }

            if (removeFunc == null)
            {
                throw new Exception("AEUtils - Error With Remover: Attached Events need remove functions");
            }

            string addName = addFunc.Method.Name;
            string removeName = removeFunc.Method.Name;

            if (!addName.StartsWith("Add") || !addName.EndsWith("Handler"))
            {
                throw new Exception("AEUtils - Error With Adder: Attached events require an add handler function whos name is formatted Add{name}Handler.");
            }

            if (!removeName.StartsWith("Remove") || !removeName.EndsWith("Handler"))
            {
                throw new Exception("AEUtils - Error With Remover: Attached events require a remove handler function whos name is formatted Remove{name}Handler.");
            }

            if (addName.Substring("Add".Length) != removeName.Substring("Remove".Length))
            {
                throw new Exception("AEUtils - Error with Adder & Remover: Attached events require matching names, name of Add{name}Handler adder function does not match Remove{name}Handler remover function.");
            }
        }
    }
}

﻿namespace AJut.Application
{
    using System;
    using System.Diagnostics;
    using System.Reflection;
    using System.Windows;
    // public static readonly RoutedEvent TapEvent = REUtils.Register(_=>_.Tap);
    //
    // private static readonly REUtilsRegistrationHelper REUtils = new REUtilsRegistrationHelper(typeof(DrawingInputSpawner));
    // public static readonly RoutedEvent TapEvent = REUtils.Register<FancyRoutedEventHandler>(nameof(TapEvent));
    // public static readonly RoutedEvent TapEvent = EventManager.RegisterRoutedEvent("Tap", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DrawingInputSpawner));
    /*
     * private static readonly REUtilsRegistrationHelper REUtils = new REUtilsRegistrationHelper(typeof(Test));
        public static readonly RoutedEvent TapEvent = REUtils.Register<MouseEventHandler>(nameof(TapEvent));
        public static readonly RoutedEvent Tap2Event = REUtils2.Register(nameof(Tap2));
     * */

    /// <summary>
    /// Instance RoutedEvent registration wrapper
    /// </summary>
    public static class REUtils<TOwner>
        where TOwner : UIElement
    {
        public static RoutedEvent Register (string name, RoutingStrategy strategy = RoutingStrategy.Bubble)
        {
            EventInfo eventField = typeof(TOwner).GetEvent (name);
            ValidateEvent (eventField);
            name = name.SubstringFromRelativeEnd(0, 5);
            return EventManager.RegisterRoutedEvent(name, strategy, eventField.EventHandlerType, typeof(TOwner));
        }

        [Conditional("DEBUG")]
        private static void ValidateEvent (EventInfo eventField)
        {
            if (eventField == null)
            {
                throw new Exception("A valid event must be provided.");
            }
        }
    }

    /// <summary>
    /// Helper class to create an instance of in your static classes who wish to have RoutedEvent wrapping
    /// </summary>
    /// <example>
    /// private static readonly REUtilsRegistrationHelper REUtils = new REUtilsRegistrationHelper(typeof(YourType));
    /// 
    /// public static readonly RoutedEvent MyCoolThingEvent = REUtils.Register(nameof(MyCoolThingEvent));
    /// </example>
    public class REUtilsRegistrationHelper
    {
        Type m_ownerType;
        public REUtilsRegistrationHelper (Type ownerType)
        {
            m_ownerType = ownerType;
        }

        public RoutedEvent Register<TEventHandler> (string name, RoutingStrategy strategy = RoutingStrategy.Bubble)
            where TEventHandler : Delegate
        {
            ValidateEventName(name);
            name = name.SubstringFromRelativeEnd(0, 5);
            return EventManager.RegisterRoutedEvent(name, strategy, typeof(TEventHandler), m_ownerType);
        }

        public RoutedEvent Register (string name, RoutingStrategy strategy = RoutingStrategy.Bubble)
        {
            ValidateEventName(name);
            name = name.SubstringFromRelativeEnd(0, 5);
            return EventManager.RegisterRoutedEvent(name, strategy, typeof(RoutedEventHandler), m_ownerType);
        }

        [Conditional("DEBUG")]
        private static void ValidateEventName (string eventName)
        {
            if (eventName == null || !eventName.EndsWith("Event"))
            {
                throw new Exception("A valid property indicator expression must be provided.");
            }
        }
    }

    public delegate void RoutedEventHandler<T> (object sender, RoutedEventArgs<T> e);
    public class RoutedEventArgs<T> : RoutedEventArgs
    {
        public RoutedEventArgs (T value)
        {
            this.Value = value;
        }
        public RoutedEventArgs (RoutedEvent routedEvent, T value) : base(routedEvent)
        {
            this.Value = value;
        }

        public RoutedEventArgs (RoutedEvent routedEvent, object source, T value) : base(routedEvent, source)
        {
            this.Value = value;
        }

        public T Value { get; }
    }
}
